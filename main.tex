\documentclass[11pt, a4paper]{article}
\usepackage{a4wide}
\usepackage[utf8]{inputenc}
\usepackage{natbib}
\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{authblk}
\usepackage{caption}
\usepackage{subcaption}
\setlength{\parindent}{0pt}
\title{\Huge Feature-Based Visual Odometry Pipeline with Self-Recorded Datasets}

\author{Luzian Bieri, Kira Erb, Antonia Mosberger, \\ Madeleine Schuler, Laura Tissari}
\affil{\textbf{Vision Algorithms for Mobile Robots} \\ \textbf{ETH Zürich}}
\date{January 2021}

\begin{document}
\maketitle

\begin{figure*}[ht]
\centering
\includegraphics[width=\textwidth]{figure/title.png}
\label{fig:universe}
\end{figure*}

\clearpage
\section{Introduction}
Mobile robots heavily rely on a precise estimation of their current pose. This pose, containing the rotation and position, is needed to either navigate in an unknown environment or to create a map. Visual odometry (VO) is the process of calculating the pose while solely relying on information obtained from the on board camera images. 
\newline
\newline
Within the scope of the course "Vision Algorithms for Mobile Robots", we implemented a feature based visual odometry pipeline for monocular vision. To initialize the pipeline, a set of keypoints with their associated 3D landmarks is extracted from two of the first few frames. Afterwards the current pose can continuously be estimated based on the extracted landmarks. Additionally, new landmarks are found and triangulated in order to replace the landmarks which travel out of the picture.
\newline
\newline
We tested our visual odometry pipeline with the three provided datasets (Parking, KITTI and Malaga). We had the possibility to record new datasets with a drone flying over the Hoenggerberg area to further test our pipeline with it. 
\paragraph{Links}
\begin{itemize}
    \item Code: \url{https://gitlab.ethz.ch/luzibier/vo-scaramuzza}
    \item Own datasets: \url{http://bit.ly/VAMR-2020-own-dataset}
    \item Screen recordings: \url{http://bit.ly/VAMR-2020-screen-records}
\end{itemize}

\section{Initialization}
\subsection{Goals}
The goal of the initialization of the VO pipeline is to extract a first set of keypoint correspondences from the two chosen keyframes and to bootstrap the initial camera pose and landmarks in a point cloud, as it was demonstrated in lecture 7. This first set of landmarks is needed since all further landmarks will rely on previous landmarks. 

\subsection{Implementation}
For the initialization we chose to bootstrap frames 0 and 2, as suggested for the KITTI dataset in the project statement, for all datasets. In order to decrease sources of error we decided to use Mathworks functions from the Computer Vision Toolbox. A new position and new landmarks are generated as follows: 

\begin{enumerate}
    \item Detect all Shi-Tomasi features of the first frame using the Matlab function which implements Shi-Tomasi \texttt{detectMinEigenFeatures} \cite{detectMinEigenFeatures}. 
    \item Select the strongest (most distinguishable) features with the function \texttt{selectStongest} \cite{selectStrongest}.
    \item Track the keypoints from the first frame in the second frame using the \texttt{vision.PointTracker} \cite{KLT}, which is based on the Kanade-Lucas-Tomasi (KLT) algorithm.
    \item Then the Mathworks function \texttt{estimateEssentialMatrix} \cite{estimateEssentialMatrix} estimates the essential matrix from the matched keypoints and the known camera intrinsics.
    \item The relative camera pose is extracted from the estimates essential matrix using the function \texttt{relativeCameraPose} \cite{relativeCameraPose}. 
    \item As a last step the matched keypoints were triangulated to a 3D point cloud of landmarks with the \texttt{triangulate} \cite{triangulate} Mathworks function. This function calculates the 3D pose of a landmark using the coordinates of a keypoint in two frames and the respective camera position.
\end{enumerate}


\subsection{Comparison of Harris, Shi-Tomasi and \texttt{detectMinEigenfeatures}}
Out of curiosity, we compared different keypoint detection methods: the two methods implemented in exercise 3, Harris and Shi-Tomasi, and the built-in Mathworks function \newline \texttt{detectMinEigenfeatures} \cite{detectMinEigenFeatures}, which is the Mathworks implementation of Shi-Tomasi. For each method, we took the 600 strongest keypoints (strongest w.r.t. the cornerness function) and matched them to the next frame by using the function \texttt{vision.PointTracker} \cite{KLT}. The number of matched keypoints indicates the performance of the keypoint detection method. Figure \ref{fig:compareKitti} and figure \ref{fig:compareMalaga} show that all three different methods to detect keypoints perform equally well. 

\begin{figure}[h]
    \centering
    \includegraphics[width=1.25\textwidth, trim=15cm 0 0 0]{figure/initialization/kitti.png}
    \caption{Comparison of feature detectors: 600 features are tracked with each method and matched to the subsequent frame with KLT. The figure shows, that approximately the same amount of matched keypoints can be extracted independent of the method.}
    \label{fig:compareKitti}
\end{figure}

\begin{figure}[h]
   \centering
    \includegraphics[width=\textwidth]{figure/initialization/malaga.png}
    \caption{Comparison of feature detectors: 600 features are tracked with each method and matched to the subsequent frame with KLT. The figure shows, that approximately the same amount of matched keypoints can be extracted independent of the method.}
    \label{fig:compareMalaga}
\end{figure}

Since none of the detectors outperformed the others, we decided to use \texttt{detectMinEigenFeatures} \cite{detectMinEigenFeatures} as feature detector, since this is a verified Matlab function that is less prone to errors and already well documented. 

\subsection{Results of Initialization}
The results of the feature detection and the feature matching can be seen can be seen in figure \ref{fig:compareKitti} and Figure \ref{fig:compareMalaga}. Possible wrong matches of the tracking are eliminated during the RANSAC step of the \texttt{estimateEssentialMatrix} step. Our bootstrap function detects a translation into the correct direction between the two frames which we could verify on all datasets.

The function was set up in a way where we could also use it in case the continuous operation failed and needed to be reinitialized. The only adjustment which needs to be done is to translate the coordinates of the landmarks to the absolute world coordinates, as the bootstrap function only returns the landmark coordinates with respect to the camera position at the first frame. Same adjustment has to be done to the pose estimation.

The results of the triangulation can be seen in the point clouds in figure \ref{fig:mhs_init}.

\subsection{Challenges}
The main challenge of the initialization was to obtain a sufficient amount of keypoints to pass on to the continuous operation. In a first iteration, we did not use Mathworks functions, but the code from the exercise sample solutions. While connecting the several building blocks, we must have made some errors, since we got way too few keypoints and corresponding landmarks (between 20 and 100). That's why we decided to start from scratch again and used Matlab functions whenever possible. 

\newpage
\section{Continuous Operation}
\subsection{Goals}
The continuous operation has two main goals. Firstly, the trajectory of the camera should be obtained. Secondly, positions of new landmarks should be obtained, since the landmarks that were detected in the initialization will be out of sight soon. In order to achieve these goals, four subtasks are defined: 
\begin{enumerate}
    \item Track the given keypoints from the last frame in the new frame. 
    \item Derive the absolute camera position w.r.t the world frame.
    \item Triangulate validated candidate keypoints in order to get new landmarks.
    \item Find new candidate keypoints which might become landmarks in the future.
\end{enumerate}

\subsection{Implementation}
As stated in the task description, the input to our function is the state of the previous frame and the previous and current image. The state contains the keypoints and landmarks of the previous frame. Additionally, the state contains the position of the candidate keypoints in the current image frame, the position of the candidate keypoints in the image frame of the first observation, and the camera positions during the first observation. 
\newline
The output of the continuous operation function, which is called for each new frame, is the updated state and the current camera position w.r.t. the world frame. Below, one iteration of the continuous operation is described. The most important of the tunable parameters are described in this report. A complete overview over all parameters can be found in the README \footnote{\url{https://gitlab.ethz.ch/luzibier/vo-scaramuzza/-/blob/master/params/README.md}}. 

\subsubsection{Point Tracking}
To track the keypoints from the last frame we use the Kanade-Lucas-Tomasi (KLT) tracker already implemented in Mathworks called \texttt{vision.PointTracker} \cite{KLT}. The following parameters are passed on to the tracker and tuned separately for each dataset: 
\begin{itemize}
    \item 'NumPyramidLevels' determines how often the downsampling takes place during the KLT algorithm. For datasets with coarse features that are not easily visible on pixel level basis (p.ex. trees on the horizon), more pyramid layers are applied. 
    \item The parameter 'MaxBidirectionalError' denotes the euclidean distance between the original keypoint and the point received by tracking the matched point from the second image back to the first. Tuning the 'MaxBidirectionalError' was a trade of: While a low value led to a more accurate trajectory, it also rejected a lot of keypoints. Therefore, we chose values between one and ten pixels. 
    \item Other parameters that can be adjusted are the descriptor block size and the maximal number of search iterations that is performed for each keypoint.
\end{itemize}
 

\subsubsection{Pose Estimation / - Refinement}
In order to calculate the position of the current frame the \texttt{estimateWorldCameraPose} \cite{estimateWorldCameraPose} Mathworks function is used which internally uses RANSAC and P3P. The most important parameter is 'MaxReprojectionError' which is used to determine outliers which then are filtered out of the set of keypoints. 
The position is optimized further using the Mathworks function \texttt{lsqnonlin} \cite{lsqnonlin} - a nonlinear least square optimizer - which minimizes the reprojection error. With this new current position the landmarks, which are now behind the camera are discarded. As the pose has 6 degrees of freedom (3 angles, 3 directions) the essential matrix has to be encoded. Euler angles seemed like an obvious solution thus the Mathworks function \texttt{eul2rotm} \cite{eultorotm} and \texttt{rot} \cite{rotmtoeul} were used to transform between angles and rotation matrices. 

\subsubsection{Candidate Keypoint Triangulation}
Current candidate keypoints are tracked in the new image using the same KLT tracker as described above. The ones that could not be tracked are discarded. Then, the remaining candidates are triangulated with the \texttt{triangulate} \cite{triangulate} function provided by Mathworks. As a result, the position of the respective landmarks are obtained in world coordinates. After that, the angle at the landmark in 3D,  between the vector to the first observation of the landmark and the vector to the current observation of the landmark is calculated and has to surpass a certain threshold.
Increasing this angle threshold led to the consequence, that landmarks far ahead of the vehicle are discarded. This is a desirable property, since these landmarks would lead to an error in the pose estimation, which would eventually lead to a failure of the pipeline. Additionally to the angle threshold, the landmarks of the candidate keypoints had to lie in front of the camera. In case both of these criteria are met, the candidate keypoint is added to the set of keypoints and the corresponding landmark is added to the set of landmarks. \\
Of course, this step is only feasible if some candidate keypoints already have been detected. 

\subsubsection{Candidate Keypoint Acquisition}
New keypoints are generated by using the \texttt{detectMinEigenFeatures} \cite{detectMinEigenFeatures} Mathworks function. A certain number of keypoints (up to 1400) among the strongest ones are selected. Keypoints which are too close to already existing ones are discarded, the other ones are saved as candidate keypoints and can  therefore be tracked and added to the keypoints in the next iteration. Figure \ref{fig:newCandidates} shows an example of this procedure. The minimal distance between keypoints ranges between five and ten pixels depending on the dataset. 
\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{figure/newCandidates.png}
    \caption{Some of the newly acquired keypoints get rejected (red) since they are too close (in this case 20 pixel) to an already existing candidate keypoint (blue). The rest of the newly acquired keypoints (green) is added to the candidate keypoints. }
    \label{fig:newCandidates}
\end{figure}

 \subsection{Challenges}
In the first iteration of our VO pipeline, we did not implement any form of refinement after calculating the current position in step 2. We were afraid that this computationally expensive function would slow down the process (which it also does). This had the effect that the pipeline failed after a few frames, since the pose error summed up. The refinement leads to a reduction of the average re-projection error.
\newline
\newline
A confusion about how we express our points (with respect to the camera or world frame?) led to many bugs in our code, which we had to exterminate one by one. For example, our pose estimation was expressed as the world origin w.r.t. the camera frame and not as expected the camera pose w.r.t. the world frame. We did not detect this problem for quite a while since we tested our pipeline with the KITTI dataset and the corresponding ground truth. In the beginning, the vehicles drives straight down the road. In this case, the rotational part of the pose is very similar to the identity matrix and we therefore did not notice, that we should invert our pose estimation. 
\newline
\newline
If for some reason the number of available keypoints is too small and/or too many keypoints are lost while tracking, do to for example too fast motion, the position can not be estimated anymore. To counteract this issue we decided to reinitialize the state with the same function used to do the initial bootstrapping once the count of keypoints falls below a threshold (usually 10). 
The same re-initialization procedure is applied when the change of position between two frames is too big. Any large change in position (threshold set via parameters) is unrealistic and should therefore be rejected. Such a situation is recognized by comparing the previous position to the estimated new position. 


\section{Additional feature}
In addition to the provided datasets we wanted to use the implemented VO pipeline on a dataset recorded with a drone following a street instead of a car. Additionally one dataset was specifically recorded in a different environment with a lot of nature (grass and trees) and one with an abrupt curve into a driveway.
Our goal was to test the robustness of the VO pipeline and compare different environments. We recorded three datasets:
\begin{itemize}
  \item Backstreet on the ETH campus Hoenggerberg with large radius curves (Hoengg 1)
  \item Backstreet on the ETH campus Hoenggerberg with a sharp turn into a driveway (Hoengg 2)
  \item Straight dirt road between a field and a sheep pasture (Sheep)
\end{itemize}

All videos were shot during cloudy days in the winter time. 

\subsection{Method}
To build our own datasets, videos have been recorded with a DJI Mavic Air drone with 25 frames per second and 720 x 1280 pixels resolution. The videos have been reduced to 640 x 360 pixels and then 25 frames per second (for the second dataset) and 4 frames per second (for the first and third dataset) have been extracted. \\

To determine the camera intrinsics, we used the inbuilt MATLAB 2020a "Single Camera Calibration" \cite{camcalib} App, a 9 x 7 checkerboard with 4 cm squares and 15 frames (2 frames per second) recorded by the drone's camera pointing at the calibration pattern.

\subsection{Discussion}

The initialization of the image sequences worked satisfactory, but produced some outliers in the point cloud. Following are the results of the continuous pipeline: 

\begin{itemize}
  \item Backstreet on the ETH campus Hoenggerberg with large radius curves (Hoengg 1, see figure \ref{fig:mhs_init}): Following a street with a big curvature, the VO pipeline is able to detect and map the trajectory. The corresponding point cloud is depicted in Figure \ref{fig:mhs_init}. At around 0:24 min the limitation of the control of the drone can be seen clearly (see section below), as the pilot corrects the path to the left and then to the right causing a bump in the trajectory. 
  \item Backstreet on the ETH campus Hönggerberg with a sharp turn into a driveway (Hoengg 2): The sharp curve is a challenge for the pipeline since almost all keypoints are lost at the same time plus the motion blur makes it hard to keep track of the remaining ones. To be able to follow the sharp left curve at around 1:40 min 25 fps (frames per second) have been used, instead of 4 fps like in the other two additional datasets. This allowed the VO pipeline to match the keypoints due to the smaller changes in the consecutive frames. Additionally the re-initialization is used. \\
  With a lot of close trees in a dataset, almost all keypoints were found in these trees. If the drones turns away from the side with the trees, all keypoints are lost since the pipeline did not track any on the other side. To counteract this behaviour we saved a higher number of keypoints and increased the minimal allowed distance between the candidate keypoints. 
  \item Straight dirt road between a field and a sheep pasture (Sheep): The grass gives a lot of texture and therefore also the possibility for quite many keypoints. However, once the drone speeds up, the keypoints in the grass are lost quickly due to  motion blur and/or too big differences between the frames and not enough new ones are regained. There are some detected keypoints in the trees far away but in the beginning of the video they are too far away so that the change in position is hard to estimate using these keypoints. Towards the end of the video the drone gets closer to the trees and these keypoints are used to compensate for the lost keypoints of the ground.
\end{itemize}

\begin{figure}
    \centering
    \includegraphics[scale=0.4]{figure/mhs_init_v3.png}
    \caption{Initialization and bootstrapping of the first dataset: Backstreet on the ETH campus Hoenggerberg with large radius curves}
    \label{fig:mhs_init}
\end{figure}


%\begin{figure}
    %\centering
    %\includegraphics[scale=0.4]{figure/lhs_init_v1.png}
    %\caption{Initialization and bootstrapping of the second data set: Backstreet on the ETH campus Hoenggerberg with a sharp turn into a driveway}
    %\label{fig:lhs_init}
%\end{figure}

%begin{figure}
    %\centering
    %\includegraphics[scale=0.2]{figure/maah_init_v2.png}
    %\caption{Initialization and bootstrapping of the third data set: Straight dirt road between a field and a sheep pasture}
    %\label{fig:maah_init}
%\end{figure}



\subsection{Challenges}

The following limitations became clear after trying it out with the VO pipeline:  \\

\textbf{Control:} The drone is controlled with two independent joysticks: One joystick controls the direction of the flight without changing the position of the camera (front-back, left-right) and the second joystick controls the height (up-down) and the rotation (yaw). The yaw control feels rather sensitive and abrupt. These abrupt movements cause enough motion blur, to be challenge to our VO pipeline. Additionally, we noticed some constant drift while flying which we tried to counteract with our steering. \\

\textbf{Speed:} The tracking and thus our VO pipeline works satisfactory as long as the drone is flying slow. If the drone is too fast the frames get blurry and the features cannot be detected and matched anymore. This can especially be seen while comparing the beginning of the dataset "Sheep" where the drone flies slowly and the end where it accelerates. See figures \ref{fig:sheep_good}, \ref{fig:sheep_getting_proken}, \ref{fig:sheep_broken} in the appendix \ref{app:appendix}.
\\

\textbf{Curves:} A drone in contrast to a car turns on the spot. This results in large change between frames and in blurry frames. In this case the features cannot be matched anymore. The same behavior can be observed once the drone moves too quickly (see above). To still be able to track enough keypoints, the ’NumPyramidLevels’ was increased. Increasing the used pyramid level enables the tracker to handle larger pixel motion, which are greater than the patch size. Since the computation cost also increases, this is a trade off. 

\section{Discussion}

\textbf{Parking:} In this dataset, especially in the beginning, almost all tracked keypoints are quite far away and therefore the pose estimation is more prone to error. A rotation is detected even though the car is driving in a straight line. This is due to scale drift in the rotational part of the pose. At minute 2:23 in the video, the pipeline resets itself since the average reprojection error after optimization has surpassed a threshold. This results in a leap in the trajectory, since the pose obtained by the re-initialization is not as accurate as a pose estimation based on keypoints that are validated over several frames. 
\newline

\textbf{KITTI:} In this dataset, some parts of the image are overexposed. Therefore finding keypoints on the building is harder and not many keypoints are found. This provokes the reset function more often than in for example the malaga dataset. The contrary effect is that in some parts of the video, half of the scenery is in the shadow (see minute 7:16). This lack of contrast makes it hard to track features. Which if too severe, can trigger the re-initialization function as well. For the KITTI dataset, the pipeline is able to detect all rotations in the right direction and even estimating the rotation at crossroads often correctly to 90 degrees. On the contrary, the estimation of the traveled distance is not really accurate. \\

\textbf{Malaga:} The VO pipeline is able to detect the driven trajectory quite good. Here, an opportunity for possible future work presents itself: the trajectory does not build a circle as it should due to the drift. Implementing a loop closure algorithm could solve this issue. A particular struggle of this dataset were the roundabouts, since many keypoints were lost there and the reset function had to be invoked often. \newline
At minute 8:33, some keypoints are detected in the clouds and even tracked over a few frames. Since no movement of these keypoints is detected due to the fact that they are far away, they are discarded soon. \\

\section{Outlook}
The current implementation of the VO pipeline works satisfactory. However, there are still issues like losing too many keypoints or a growing reprojection error. The following extensions of the pipeline could lead to significant improvements:

\begin{itemize}
    \item For the initialization we chose to bootstrap frames 0 and 2 as keyframes. This was suggested for the KITTI dataset and we used it for all datasets. The keyframe selection could be optimized for each dataset and so improve the initial pose.
    \item The reprojection error is used to minimize the error in rotation and translation. Bundle adjustment could be used in order to also optimize the 3D landmarks and therefore minimized the error which is added up over the whole trajectory. 
    \item Improve the selection of new candidate keypoints: Currently we select a fixed amount of the strongest keypoints and then reject the ones which are too close to the existing candidates and keypoints. However, the reverse sequence would be favourable as this would ensure an addition of a constant amount of keypoints.
    \item Before adding a candidate landmark to the landmark list it would be reasonable to check whether the landmarks are triangulated to a "{}reasonable"{} position. E.g. reject landmarks which are further away than a certain threshold. 
    \item In order to minimize the overall drift of the pipeline loop closure could be implemented, by using place recognition. 
    \item Since the three provided datasets are all recorded by a car, it is possible to take the motion constraints of the vehicle into account. In this case, the pose estimation could be based on two points, instead of four like implemented in \texttt{estimateWorldCameraPose} \cite{estimateWorldCameraPose}, as it was demonstrated in lecture 9.  This would increase the efficiency of the pipeline, since the RANSAC algorithm would be a lot faster and therefore more iterations could be used.Using this assumption based on ackermann steering would only be feasible for the datasets recorded by a car and not the drone.
    \item To additionally speed up the implementation of the VO pipline multi-threading should be considered, since at the moment only one thread is being used.
\end{itemize}

\newpage

\bibliographystyle{IEEEtran}
\bibliography{references}

\newpage
\appendix

% \section{Initialization Keypoints: Additional Figures}

%In this section we included the figures of the keypoint initialization, which were not relevant for the report.

%\begin{figure}[h!]
%    \centering
%    \includegraphics[scale=0.38]{figure/kitti_madi.eps}
%    \caption{Keypoint Initialization of the KITTI data set.}
%    \label{fig:kitti_madi}
%\end{figure}

%\begin{figure}[h!]
%    \centering
%    \includegraphics[scale=0.38]{figure/parking_madi.eps}
%    \caption{Keypoint Initialization of the Parking data set.}
%    \label{fig:parking_madi}
%\end{figure}

%\begin{figure}[h!]
%    \centering
%    \includegraphics[scale=0.38]{figure/mhs_madi.eps}
%    \caption{Keypoint Initialization of the Hoengg 1 data set.}
%    \label{fig:mhs_madi}
%\end{figure}

%\begin{figure}[h!]
%    \centering
%    \includegraphics[scale=0.38]{figure/lhs_madi.eps}
%    \caption{Keypoint Initialization of the Hoengg 2 data set.}
%    \label{fig:lhs_madi}
%\end{figure}

%\newpage
%\newpage

\section{Additional Figures}\label{app:appendix}

%\begin{figure}[h!]
%    \centering
%    \includegraphics[scale=0.38]{figure/kitti_init.eps}
%    \caption{Initialization of VO for the KITTI data set.}
%    \label{fig:mhs_madi}
%\end{figure}

%\begin{figure}[h!]
%    \centering
%    \includegraphics[scale=0.38]{figure/malaga_init.eps}
%    \caption{Initialization of VO for the Malaga data set.}
%    \label{fig:lhs_madi}
%\end{figure}

%\begin{figure}[h!]
%    \centering
%    \includegraphics[scale=0.38]{figure/parking_init.eps}
%    \caption{Initialization of VO for the Parking data set.}
%    \label{fig:lhs_madi}
%\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=1.25\textwidth, trim=20cm 0 0 0]{figure/initialization/sheep.png}
    \caption{Comparison of feature detectors: 600 features are tracked with each method and matched to the subsequent frame with KLT. All three detectors work well on the self-recorded dataset, which consists of a lot of nature (trees and grass) in comparison to the other datasets.}
    \label{fig:compareSheep}
\end{figure}

\begin{figure}[h]
 \centering
 \includegraphics[width=1.0\textwidth]{figure/sheep/schäfli_good.png}
 \caption{Good motion tracking while the drone flies slow}
 \label{fig:sheep_good}
\end{figure}
\hfill
\begin{figure}[h]
 \centering
 \includegraphics[width=1.0\textwidth]{figure/sheep/schäfli_getting proken.png}
 \caption{Trajectory turns imprecise as the drone accelerates}
 \label{fig:sheep_getting_proken}
\end{figure}
\hfill
\begin{figure}[h]
 \centering
 \includegraphics[width=1.0\textwidth]{figure/sheep/schäfli_broken.png}
 \caption{Large drift as drone speed gets large. Only keypoints further away are trackable}
 \label{fig:sheep_broken}
\end{figure}

\end{document}
